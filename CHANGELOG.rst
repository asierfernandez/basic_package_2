^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package basic_package
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.0.3 (2018-06-25)
------------------
* fix executable name
* Contributors: Asier Fernandez

0.0.2 (2018-06-25)
------------------
* rename package name
* Contributors: Asier Fernandez

0.0.1 (2018-06-22)
------------------
* initial commit
* Contributors: Asier Fernandez
