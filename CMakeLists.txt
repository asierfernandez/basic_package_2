cmake_minimum_required(VERSION 2.8.3)
project(basic_package_2)

find_package(catkin REQUIRED
	roscpp
	)

catkin_package()

include_directories(
  ${catkin_INCLUDE_DIRS}
)

add_executable(basic_package_node src/basic_package.cpp)
target_link_libraries(basic_package_node
   ${catkin_LIBRARIES}
)

install(TARGETS basic_package_node
   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)